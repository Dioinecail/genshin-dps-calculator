// JavaScript source code

var attackBase = 500;
var attackBonus = 0.5;
var attackFlat = 500;
var abilityDMG = 1;
var dmgBonus = 0.3;
var critDMG = 0.5;
var ATKTotal;
var enemyRes = 0.1;

onAttackBaseChanged();
onAttackBonusChanged();
onAttackFlatChanged();
onCritDmgChanged();
onDmgBonusChanged();
onAbilityDmgChanged();
DEBUG_CalculateDmg();

function DEBUG_CalculateDmg()
{
    ATKTotal = CalculateAttackTotal(attackBase, attackBonus, attackFlat);
    var outgoingDmgCrit = OutgoingDMG(ATKTotal, abilityDMG, dmgBonus, true, critDMG);
    var outgoingDmgNonCrit = OutgoingDMG(ATKTotal, abilityDMG, dmgBonus, false, critDMG);

    var resMult = ResMultiplier(enemyRes, 0, 0);

    var incomingDmgCrit = IncomingDMG(outgoingDmgCrit, 90, 90, 0, resMult);
    var incomingDmgNonCrit = IncomingDMG(outgoingDmgNonCrit, 90, 90, 0, resMult);

    var atkString = attackBase.toString();
    atkString = atkString.concat('+');
    atkString = atkString.concat((attackBase * attackBonus).toString());
    atkString = atkString.concat('+');
    atkString = atkString.concat(attackFlat.toString());
    atkString = atkString.concat('=');
    atkString = atkString.concat(ATKTotal.toString());

    document.getElementsByName("outgoing-value-crit")[0].innerText = Math.round(outgoingDmgCrit).toString();
    document.getElementsByName("outgoing-value-non-crit")[0].innerText = Math.round(outgoingDmgNonCrit).toString();
    document.getElementsByName("incoming-value-crit")[0].innerText = Math.round(incomingDmgCrit).toString();
    document.getElementsByName("incoming-value-non-crit")[0].innerText = Math.round(incomingDmgNonCrit).toString();
    document.getElementsByName("atkTotal")[0].innerText = atkString;
}

function onAttackBaseChanged()
{
    attackBase = +document.getElementsByName("atkBase")[0].value;
    DEBUG_CalculateDmg();
}
function onAttackBonusChanged()
{
    attackBonus = +document.getElementsByName("attackBonus")[0].value / 100;
    DEBUG_CalculateDmg();
}
function onAttackFlatChanged()
{
    attackFlat = +document.getElementsByName("attackFlat")[0].value;
    DEBUG_CalculateDmg();
}
function onAbilityDmgChanged()
{
    abilityDMG = +document.getElementsByName("abilityDMG")[0].value / 100;
    DEBUG_CalculateDmg();
}
function onDmgBonusChanged()
{
    dmgBonus = +document.getElementsByName("dmgBonus")[0].value;
    DEBUG_CalculateDmg();
}
function onCritDmgChanged()
{
    critDMG = +document.getElementsByName("critDMG")[0].value / 100;
    DEBUG_CalculateDmg();
}
function onEnemyResChanged()
{
    enemyRes = +document.getElementsByName("enemyRes")[0].value / 100;
    DEBUG_CalculateDmg();
}

function OutgoingDMG(ATK, percentAbility, dmgBonus, crit, critDmg)
{
    var totalDmgBonus = 1 + (dmgBonus / 100);

    var dmg = ATK * percentAbility * totalDmgBonus;

    if (crit)
    {
        return dmg * (1 + critDmg);
    }
    else
    {
        return dmg;
    }
}

function CalculateAttackTotal(atkBase, percentAtk, atkFlat)
{
    return (atkBase * (1 + percentAtk)) + atkFlat;
}

function IncomingDMG(outgoingDMG, enemyLevel, charLevel, defReduction, resistanceMult)
{
    var defMult = (charLevel + 100) / (((1 - defReduction) * enemyLevel) + charLevel + 200);

    return outgoingDMG * defMult * resistanceMult;
}

function ResMultiplier(resBase, resBonus, resDebuff)
{
    var resistance = resBase + resBonus + resDebuff;

    if (resistance < 0)
    {
        return 1 - (resistance / 2);
    }
    else if (resistance >= 0 && resistance < 0.75)
    {
        return 1 - resistance;
    }
    else
    {
        return 1 / ((4 * resistance) + 1);
    }
}

function TransformativeReaction(baseMult, charLevel, elementalMastery, reactionBonus, resMult)
{
    var EMBonus = (16 * (elementalMastery / (elementalMastery + 2000))) / 100;

    return baseMult * charLevel * (1 + EMBonus + reactionBonus) * resMult;
}

function AmplifyingReaction(isMelt, isPyro, incomingDmg, elementalMastery, reactionBonus)
{
    var reactionMult = 1;

    if (isMelt)
    {
        if (isPyro)
            reactionMult = 2;
        else
            reactionMult = 1.5;
    }
    else
    {
        if (isPyro)
            reactionMult = 1.5;
        else
            reactionMult = 2;
    }

    var EMBonus = (2.78 * (elementalMastery / (elementalMastery + 1400))) / 100;
    var amplifyingMult = reactionMult * (1 + EMBonus + reactionBonus);

    return incomingDmg * amplifyingMult;
}